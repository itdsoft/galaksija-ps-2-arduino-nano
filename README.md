# Galaksija PS/2 Arduino Nano

PS/2 keyboard interface based on Arduino Nano and MT8808 keyboard matrix IC for the Galaksija Microcomputer

Author: Dragan Toroman

LATEST PCB VERSION 1.2. BOTH CMOS AND FIFAN SHOULD WORK USING THE SAME Ps2Galaksija software

NOTE: FOR THOSE WHO DOWNLOADED AND PRODUCED THE V1.1 PCB - THAT ONE HAD DIFFERENT WIRING FOR CMOS AND FIFAN MODE, SO FOR FIFAN USE THE FIFAN 1.1 SOFTWARE - Ps2Galaksija-patchfifan-forV1.1 and for CMOS Ps2Galaksija

##Dependances:
*   Ps2KeyboardHost by Steve Benz
*   FastGPIO by Pololu

both libraries can be installed directly from the Arduino IDE

THE SAME PCB AND SOFTWARE CAN BE USED FOR ANY GALAKSIJA COMPUTER. DIRECT PLUG-IN SUPPORT FOR TOMAŽ ŠOLC CMOS GALAKSIJA AND FIFAN GALAKSIJA

##BOM
*   U2  - MT8808AE
*   U3  - Arduino NANO
*   RN1 - Resistor Network 10K A103J (for FIFAN GALAKSIJA this part is not mandatory - as there is already a resistor network on the main board)
*   D1-D8 - 1N4148
*   PS/2 female connector

###If used for Tomaž Šolc CMOS Galaksija:
*   2x10 female header*

###If used for FIFAN Galaksija Plus
*   XP2, XP3 - 1x8 Male header*
*   1x2 pin header for power supply (+5V, GND)**



*Headers mounted on bottom of PCB

**Marked on the CMOS header


