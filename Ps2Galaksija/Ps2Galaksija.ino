/*
PS/2 keyboard interface based on Arduino Nano and MT8808 keyboard matrix IC for the Galaksija Microcomputer
PCB V1.1 CMOS
PCB V1.2 CMOS AND FIFAN
Author: Dragan Toroman
*/
#include <FastGPIO.h>
#include "ps2_Keyboard.h"
#include "ps2_AnsiTranslator.h"
#include "ps2_SimpleDiagnostics.h"

// Pins assigned to address lines AX0-AX2 and AY0-AY2
const int AX0 = 5; const int AX1 = 6; const int AX2 = 7;
const int AY0 = 8; const int AY1 = 9; const int AY2 = 10;
const int DAT = 13;   // Data
const int STR = 12;   // Strobe
const int RES = 11;   // Reset
bool keyrelease = false;
bool extrelease = false;
bool wasakey = false;
typedef ps2::SimpleDiagnostics<254> Diagnostics_;
static Diagnostics_ diagnostics;
static ps2::AnsiTranslator<Diagnostics_> keyMapping(diagnostics);
static ps2::Keyboard<4,3,1, Diagnostics_> ps2Keyboard(diagnostics);
static ps2::KeyboardLeds lastLedSent = ps2::KeyboardLeds::none;
void setup() {
    //Serial.begin(115200);
    FastGPIO::Pin<AX0>::setOutputLow(); FastGPIO::Pin<AX1>::setOutputLow(); FastGPIO::Pin<AX2>::setOutputLow();
    FastGPIO::Pin<AY0>::setOutputLow(); FastGPIO::Pin<AY1>::setOutputLow(); FastGPIO::Pin<AY2>::setOutputLow();
    FastGPIO::Pin<DAT>::setOutputLow(); FastGPIO::Pin<STR>::setOutputLow(); FastGPIO::Pin<RES>::setOutputHigh();
    delay(20);
    FastGPIO::Pin<RES>::setOutputValueLow();
    ps2Keyboard.reset();
    ps2Keyboard.begin();
    delay(300);
    //keyMapping.setNumLock(true);
    ps2Keyboard.awaitStartup();
    diagnostics.reset();
    delay(300);
    ps2Keyboard.begin();
    keyMapping.setNumLock(true);
    ps2Keyboard.awaitStartup();
    delay(300);
    ps2Keyboard.setScanCodeSet(ps2::ScanCodeSet::pcat);
    //ps2Keyboard.setScanCodeSet(ps2::ScanCodeSet::ps2);
    ps2Keyboard.sendLedStatus(ps2::KeyboardLeds::numLock);
    lastLedSent = ps2::KeyboardLeds::numLock;
    resetOnly();
}

void strobeReset () {
  FastGPIO::Pin<DAT>::setOutputValueHigh();
  FastGPIO::Pin<STR>::setOutputValueHigh();
  delay(10);
  FastGPIO::Pin<STR>::setOutputValueLow();
  delay(10);
  FastGPIO::Pin<RES>::setOutputValueHigh();
  FastGPIO::Pin<RES>::setOutputValueLow();
}

void strobeDataHigh () {
    FastGPIO::Pin<DAT>::setOutputValueHigh();
    FastGPIO::Pin<STR>::setOutputValueHigh();
    FastGPIO::Pin<STR>::setOutputValueLow();
}

void strobeDataLow () {
  FastGPIO::Pin<DAT>::setOutputValueLow();
  FastGPIO::Pin<STR>::setOutputValueHigh();
  FastGPIO::Pin<STR>::setOutputValueLow();
}

void strobeOnly () {
  FastGPIO::Pin<DAT>::setOutputValueHigh();
  FastGPIO::Pin<STR>::setOutputValueHigh();
  FastGPIO::Pin<STR>::setOutputValueLow();
}


void resetOnly () {
  FastGPIO::Pin<RES>::setOutputValueHigh();
  delay(20);
  FastGPIO::Pin<RES>::setOutputValueLow();
}

void sendAddress (byte code) {
  FastGPIO::Pin<AY2>::setOutputValue(HIGH && (code & B00100000));
  FastGPIO::Pin<AY1>::setOutputValue(HIGH && (code & B00010000));
  FastGPIO::Pin<AY0>::setOutputValue(HIGH && (code & B00001000));
  FastGPIO::Pin<AX2>::setOutputValue(HIGH && (code & B00000100));
  FastGPIO::Pin<AX1>::setOutputValue(HIGH && (code & B00000010));
  FastGPIO::Pin<AX0>::setOutputValue(HIGH && (code & B00000001));
}

void loop() {
    diagnostics.setLedIndicator<LED_BUILTIN>();
    ps2::KeyboardOutput scanCode = ps2Keyboard.readScanCode();
    if (scanCode == ps2::KeyboardOutput::garbled) {
        keyMapping.reset();
    }
    else if (scanCode != ps2::KeyboardOutput::none)
    {
        char buf;
        buf = keyMapping.translatePs2Keycode(scanCode);
        if (buf == '\004') { // ctrl+D
            //Serial.println();
            //diagnostics.sendReport(Serial);
            //Serial.println();
            diagnostics.reset();
        }
        else {
            //Serial.print("CONTROL: ");
            //Serial.println((byte)scanCode, HEX);
            if ((byte)scanCode == 0xE0) { //Extended
              diagnostics.setLedIndicator<LED_BUILTIN>();
              delay(3);
              scanCode = ps2Keyboard.readScanCode();
              if ((byte)scanCode == 0xF0) { //EXTRELEASE
                extrelease = true; 
                diagnostics.setLedIndicator<LED_BUILTIN>();
                delay(3);
                scanCode = ps2Keyboard.readScanCode();
              }
                switch ((byte)scanCode) {
                  case 0x6B: //Left Arrow
                    sendAddress(0x2B);
                    wasakey=true;
                    break;
                  case 0x74: //Right Arrow
                    sendAddress(0x33);
                    wasakey=true;
                    break;
                  case 0x75: //Up Arrow
                    sendAddress(0x1B);
                    wasakey=true;
                    break;
                  case 0x72: //Down Arrow
                    sendAddress(0x23);
                    wasakey=true;
                    break;
                  case 0x4A: // /
                    sendAddress(0x3D);
                    wasakey=true;
                    break;
                  case 0x5A: // RETURN
                    sendAddress(0x06);
                    wasakey=true;
                    break;
                  case 0x71 :// DEL
                    sendAddress(0x1E);
                    wasakey = true;
                    break;
                  case 0x7A :// PgDown LIST
                    sendAddress(0x26);
                    wasakey = true;
                    break;
                }
                if (wasakey) {
                  if (extrelease) {
                    strobeDataLow();
                  } else {
                    strobeDataHigh();
                  }
                }
              wasakey = false;
              extrelease = false;
            }
            
            else {
              if ((byte)scanCode == 0xF0) { //RELEASE
                keyrelease = true;
                diagnostics.setLedIndicator<LED_BUILTIN>();
                delay(3);
                scanCode = ps2Keyboard.readScanCode();
              }
              // PRESS
                  switch ((byte)scanCode) {
                    case 0x66 : // Backspace LEFT ARROW
                      sendAddress(0x2B);
                      wasakey = true;
                      break;                    
                    case 0x1C : // A
                      sendAddress(0x08);
                      wasakey = true;
                      break;
                    case 0x32 : // B
                      sendAddress(0x10);
                      wasakey = true;
                      break;
                    case 0x21 : // C
                      sendAddress(0x18);
                      wasakey = true;
                      break;
                    case 0x23 : // D
                      sendAddress(0x20);
                      wasakey = true;
                      break;
                    case 0x24 : // E
                      sendAddress(0x28);
                      wasakey = true;
                      break;
                    case 0x2B : // F
                      sendAddress(0x30);
                      wasakey = true;
                      break;
                    case 0x34 : // G
                      sendAddress(0x38);
                      wasakey = true;
                      break;
                    case 0x33 : // H
                      sendAddress(0x01);
                      wasakey = true;
                      break;
                    case 0x43 : // I
                      sendAddress(0x09);
                      wasakey = true;
                      break;
                    case 0x3B : // J
                      sendAddress(0x11);
                      wasakey = true;
                      break;
                    case 0x42 : // K
                      sendAddress(0x19);
                      wasakey = true;
                      break;
                    case 0x4B : // L
                      sendAddress(0x21);
                      wasakey = true;
                      break;
                    case 0x3A : // M
                      sendAddress(0x29);
                      wasakey = true;
                      break;
                    case 0x31 : // N
                      sendAddress(0x31);
                      wasakey = true;
                      break;
                    case 0x44 : // O
                      sendAddress(0x39);
                      wasakey = true;
                      break;
                    case 0x4D : // P
                      sendAddress(0x02);
                      wasakey = true;
                      break;
                    case 0x15 : // Q
                      sendAddress(0x0A);
                      wasakey = true;
                      break;
                    case 0x2D : // R
                      sendAddress(0x12);
                      wasakey = true;
                      break;
                    case 0x1B : // S
                      sendAddress(0x1A);
                      wasakey = true;
                      break;
                    case 0x2C : // T
                      sendAddress(0x22);
                      wasakey = true;
                      break;
                    case 0x3C : // U
                      sendAddress(0x2A);
                      wasakey = true;
                      break;
                    case 0x2A : // V
                      sendAddress(0x32);
                      wasakey = true;
                      break;
                    case 0x1D : // W
                      sendAddress(0x3A);
                      wasakey = true;
                      break;
                    case 0x22 : // X
                      sendAddress(0x03);
                      wasakey = true;
                      break;
                    case 0x35 : // Y
                      sendAddress(0x0B);
                      wasakey = true;
                      break;
                    case 0x1A : // Z
                      sendAddress(0x13);
                      wasakey = true;
                      break;
                    case 0x29 : // SPACE
                      sendAddress(0x3B);
                      wasakey = true;
                      break;
                    case 0x45 : case 0x70 : // 0
                      sendAddress(0x04);
                      wasakey = true;
                      break;
                    case 0x16 : case 0x69 : // 1
                      sendAddress(0x0C);
                      wasakey = true;
                      break;
                    case 0x1E : case 0x72: // 2
                      sendAddress(0x14);
                      wasakey = true;
                      break;
                    case 0x26 : case 0x7A : // 3
                      sendAddress(0x1C);
                      wasakey = true;
                      break;
                    case 0x25 : case 0x6B : // 4
                      sendAddress(0x24);
                      wasakey = true;
                      break;
                    case 0x2E : case 0x73 : // 5
                      sendAddress(0x2C);
                      wasakey = true;
                      break;
                    case 0x36 : case 0x74 : // 6
                      sendAddress(0x34);
                      wasakey = true;
                      break;
                    case 0x3D : case 0x6C : // 7
                      sendAddress(0x3C);
                      wasakey = true;
                      break;
                    case 0x3E : case 0x75 : // 8
                      sendAddress(0x05);
                      wasakey = true;
                      break;
                    case 0x46 : case 0x7D : // 9
                      sendAddress(0x0D);
                      wasakey = true;
                      break;
                    case 0x4C :// ;
                      sendAddress(0x15);
                      wasakey = true;
                      break;
                    case 0x52 :// :
                      sendAddress(0x1D);
                      wasakey = true;
                      break;
                    case 0x41 :// ,
                      sendAddress(0x25);
                      wasakey = true;
                      break;
                    case 0x55 :// =
                      sendAddress(0x2D);
                      wasakey = true;
                      break;
                    case 0x49 : case 0x71 :// .
                      sendAddress(0x35);
                      wasakey = true;
                      break;
                    case 0x4A :// /
                      sendAddress(0x3D);
                      wasakey = true;
                      break;
                    case 0x5A :// RETURN
                      sendAddress(0x06);
                      wasakey = true;
                      break;
                    case 0x76 :// ESC BREAK
                      sendAddress(0x0E);
                      wasakey = true;
                      break;
                    case 0x0D :// TAB REPEAT
                      sendAddress(0x16);
                      wasakey = true;
                      break;
                    case 0x12 : case 0x59 :// SHIFT
                      sendAddress(0x2E);
                      wasakey = true;
                      break;
                  } //switch
                  if (wasakey) {
                    if (keyrelease) {
                      strobeDataLow();
                      //Serial.println("RELEASE");
                    } else {
                      strobeDataHigh();
                      //Serial.println("PRESS");
                    }
                  }
              wasakey = false;
              keyrelease = false;
          }
        }
          ps2::KeyboardLeds newLeds =
              (keyMapping.getCapsLock() ? ps2::KeyboardLeds::capsLock : ps2::KeyboardLeds::none)
            | (keyMapping.getNumLock() ? ps2::KeyboardLeds::numLock : ps2::KeyboardLeds::none);
          if (newLeds != lastLedSent) {
            ps2Keyboard.sendLedStatus(newLeds);
            lastLedSent = newLeds;
          }
    }
}
